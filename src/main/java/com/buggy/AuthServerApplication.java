package com.buggy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

@SpringBootApplication
public class AuthServerApplication {


    public static void main(String[] args) {
        SpringApplication.run(AuthServerApplication.class, args);
//		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//		System.out.println(passwordEncoder.encode("1234"));
//		System.out.println(passwordEncoder.matches("1234","$2a$10$UzvNsiQ9WxPxM/DWfOJl1e8VKx1sO/sjh.JBlD80fRJ1BLW.E/M8W"));
    }

}
